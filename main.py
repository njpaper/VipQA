import logging
from settings import settings

from app_init import create_application

import colorama

# 初始化 colorama 模块 在控制台、命令行输出彩色文字的模块,可以跨平台使用
colorama.init(autoreset=True)

app = create_application()
# app = FastAPI()

if __name__ == "__main__":
    import uvicorn

    # 设置日志级别
    logging.root.setLevel(logging.INFO)
    logging.info("Starting on  %s:%d ", settings.APP_HOST, settings.APP_PORT)

    # main:app main下面的 app，相当于注入
    # main: main.py 文件(也可理解为Python模块).
    # app: main.py 中 app = FastAPI()
    # 语句创建的app对象.
    # --reload: 在代码改变后重启服务器，只能在开发的时候使用
    uvicorn.run("main:app", host=settings.APP_HOST, port=settings.APP_PORT)
