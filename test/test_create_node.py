import logging
from utils.neo4j_provider import driver

logging.root.setLevel(logging.INFO)

''' 创建知识图谱实体节点类型schema '''


def create_drug(tx, name):
    query = (
        "CREATE (n:Drug {name: $name})"
        "RETURN id(n)"
    )
    result = tx.run(query, name=name)
    return result.single()[0]


if __name__ == "__main__":
    with driver.session() as session:
        session.execute_write(create_drug, "扶他林")
    driver.close()
    logging.info("创建成功")
