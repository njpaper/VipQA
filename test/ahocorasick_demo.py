# coding:utf-8
import ahocorasick


def make_AC(AC, word_set):
    for word in word_set:
        AC.add_word(word, word) # 向trie树中添加单词
    return AC


def ac_demo():
    '''
    ahocosick：自动机的意思
    可实现自动批量匹配字符串的作用，即可一次返回该条字符串中命中的所有关键词
    '''
    key_list = ["胀痛", "看东西有时候清楚有时候不清楚", "畏光"]
    AC_KEY = ahocorasick.Automaton()
    AC_KEY = make_AC(AC_KEY, set(key_list))
    AC_KEY.make_automaton()
    test_str_list = ["请问最近看东西有时候清楚有时候不清楚是怎么回事", "有时候眼睛胀痛，畏光"]
    for content in test_str_list:
        name_list = set()
        for item in AC_KEY.iter(content):  # 将AC_KEY中的每一项与content内容作对比，若匹配则返回
            name_list.add(item[1])
        name_list = list(name_list)
        if len(name_list) > 0:
            print("\n", content, "--->命中的关键词有：", "、".join(name_list))


if __name__ == "__main__":
    ac_demo()
