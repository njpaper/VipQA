import csv
import json

# 读取CSV文件
with open('../db/result.csv', 'r', encoding='utf-8') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader:
        print(row)
        print(row['姓名'])
    data = [row for row in csv_reader]

# 将数据转换为JSON格式
"""
ensure_ascii 中文直接显示
indent=4 格式化，参数则使生成的JSON格式更美观。
"""
json_data = json.dumps(data, ensure_ascii=False, indent=4)

print(json_data)

# 将JSON数据写入文件
with open('../db/output.json', 'w', encoding='utf-8') as json_file:
    json_file.write(json_data)
