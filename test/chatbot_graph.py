from service.question_classifier import *
from service.question_parser import *
from service.answer_search import *
import colorama
from colorama import Fore, Back, Style


class ChatBotGraph:
    """
    问答类
    """

    def __init__(self):
        self.classifier = QuestionClassifier()
        self.parser = QuestionPaser()
        self.searcher = AnswerSearcher()

    def chat_main(self, sent):
        answer = '您的问题，我还没有学习到。祝您身体健康！'
        res_classify = self.classifier.classify(sent)
        if not res_classify:
            return answer
        res_sql = self.parser.parser_main(res_classify)
        final_answers = self.searcher.search_main(res_sql)
        if not final_answers:
            return answer
        else:
            return '\n'.join(final_answers)


if __name__ == '__main__':
    print("VipQA：您好，我是人工智能助理，希望可以帮到您")
    handler = ChatBotGraph()
    while 1:
        colorama.init()
        question = input(Fore.WHITE + '用户：')  # 请问最近看东西有时候清楚有时候不清楚是怎么回事，干眼常用药有哪些，干眼哪些不能吃
        answer = handler.chat_main(question)
        log_msg = f"医生：{Fore.CYAN}{answer} \n"  # 青色灰底
        print(log_msg)
