import pandas as pd

df = pd.read_csv('../db/symptom_data.csv')

symptoms_extend = []
symptoms_append = []
for idx, each in enumerate(df['症状']):
    sp = each.split(',')
    symptoms_extend.extend(sp)  # 在末尾追加序列的值 结果为 => [X1，X2,X3，X4]
    symptoms_append.append(sp)  # 在末尾追加对接，附加在里面 结果为 => [[X1，X2],[X3，X4]]
    print("%s sp => %s" % (idx, sp))
    print("%s extend => %s" % (idx, symptoms_extend))
    print("%s append => %s" % (idx, symptoms_append))
    print('--' * 20)

print("extend => %s" % (symptoms_extend))
print("append => %s" % (symptoms_append))
