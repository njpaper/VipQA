from dotenv import dotenv_values
from typing import List

dotenv_config = dotenv_values('.env')


class Settings:
    BACKEND_CORS_ORIGINS: List = ['*']

    # APP
    APP_HOST = dotenv_config.get("APP_HOST", "127.0.0.1")
    APP_PORT = int(dotenv_config.get("APP_PORT", 8000))

    # Neo4j
    NEO4J_URI = dotenv_config.get("NEO4J_URI", "neo4j://172.16.3.64:7687")
    NEO4J_USER = dotenv_config.get("NEO4J_USER", "neo4j")
    NEO4J_PASSWORD = dotenv_config.get("NEO4J_PASSWORD", "password")
    NEO4J_VERSION = dotenv_config.get("NEO4J_VERSION", "5")
    NEO4J_DATABASE = dotenv_config.get("NEO4J_DATABASE", "neo4j")
    NEO4J_PORT = int(dotenv_config.get("NEO4J_PORT", 8080))


settings = Settings()
