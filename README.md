# VipQA

#### 介绍
医疗在线问诊

参考：https://github.com/liuhuanyong/QASystemOnMedicalKG


#### 软件架构
软件架构说明


#### 知识图谱构建框架

![](docs/images/kg_route.png)

#### 构建节点
db/build_nodes.py

创建7个标签对应的节点

| 实体类型 | 中文含义 | 举例 |
| :--- | :---: | :---: | :--- |
| Check | 诊断检查项目 | 支气管造影;关节镜检查|
| Disease | 疾病 |  血栓闭塞性脉管炎;胸降主动脉动脉瘤|
| Drug | 药品 | 京万红痔疮膏;布林佐胺滴眼液|
| Food | 食物 | 番茄冲菜牛肉丸汤;竹笋炖羊肉|
| Producer | 在售药品 | 通药制药青霉素V钾片;青阳醋酸地塞米松片|
| Symptom | 症状 | 乳腺组织肥厚;脑实质深部出血| 