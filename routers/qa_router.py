#!/usr/bin/python3

import logging
from fastapi import APIRouter, status
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from service.qa_service import QAService
import json

router = APIRouter()
qa = QAService()


class Item(BaseModel):
    name: str = None
    question: str


@router.post("/consult")
async def get_search(param: Item):
    answer = qa.chat_main(param.question)
    return JSONResponse(content=answer, status_code=status.HTTP_200_OK)
