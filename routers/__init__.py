from fastapi import APIRouter
from service.question_classifier import *
from service.question_parser import *
from service.answer_search import *

from . import node_router
from . import qa_router

api_router = APIRouter()

# tags 显示在 Swagger 上的标题
api_router.include_router(node_router.router, tags=['Node'], prefix='/node')
api_router.include_router(qa_router.router, tags=['Node'], prefix='/qa')

classifier = QuestionClassifier()
parser = QuestionPaser()
searcher = AnswerSearcher()
